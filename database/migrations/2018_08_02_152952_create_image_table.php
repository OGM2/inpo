<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateImageTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('inpo_image', function (Blueprint $table) {
            $table->charset = 'utf8';
            $table->collation = 'utf8_czech_ci';
            $table->increments('id');
            $table->string('filename', 250);
            $table->string('path', 500);
            $table->integer('position');
            $table->integer('article_id')->unsigned();
            $table->timestamps();
        });

        Schema::table('inpo_image', function (Blueprint $table) {
            $table->foreign('article_id')
                ->references('id')->on('inpo_article')
                ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('inpo_image');
    }
}
