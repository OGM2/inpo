<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateArticleTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('inpo_article', function (Blueprint $table) {
            $table->charset = 'utf8';
            $table->collation = 'utf8_czech_ci';
            $table->increments('id');
            $table->string('name', 250);
            $table->string('slug', 250);
            $table->text('perex')->nullable();
            $table->text('content');
            $table->text('reference')->nullable();
            $table->integer('position');
            $table->integer('parent')->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('inpo_article');
    }
}
