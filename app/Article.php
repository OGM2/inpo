<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Article extends Model
{

	protected $table = 'inpo_article';

	protected $guarded = ['id'];

	public function getHiearchy()
	{
		$articles = Article::select('id', 'parent', 'position', 'name', 'slug')->get();

		$keyed = $articles->mapWithKeys(function ($item) 
		{
			return [$item['id'] => array('parent' => $item['parent'], 'position' => $item['position'], 'name' => $item['name'], 'slug' => $item['slug'])];
		});

		return $keyed->all();
	}

	public function getArticle($slug)
	{
		$article = Article::select('*')->where('slug', $slug)->first();

		return $article;
	}

}
