<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Article;
use App\Traits\ArticleStructure;

class HomeController extends Controller
{
    use ArticleStructure;

    protected $article;

    public $hiearchy;

    public function __construct(Article $article)
    {
    	$this->article = $article;
        $this->hiearchy = $this->article->getHiearchy();
    }

    public function index()
    {
        $tree = $this->getStructure($this->hiearchy);

        return view('homepage')->with(compact('tree'));
    }

}
