<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Traits\ArticleStructure;
use App\Article;

class ArticleController extends Controller
{
	use ArticleStructure;

	protected $article;

	public $hiearchy;

	public function __construct(Article $article)
	{
		$this->article = $article;
		$this->hiearchy = $this->article->getHiearchy();
	}

    public function index($slug)
    {
    	$article = $this->article->getArticle($slug);
        $tree = $this->getStructure($this->hiearchy);

    	return view('article')->with(compact(['article', 'tree']));
    }
}
