<?php

namespace App\Admin\Controllers;

use App\Article;
use App\Http\Controllers\Controller;
use Encore\Admin\Controllers\ModelForm;
use Encore\Admin\Form;
use Encore\Admin\Grid;
use Encore\Admin\Facades\Admin;
use Encore\Admin\Layout\Content;
use Encore\Admin\Show;
use Illuminate\Http\Request;

class ArticleController extends Controller
{
    use ModelForm;

    protected $article;

    public function __construct(Article $article)
    {
        $this->article = $article;
    }

    /**
     * Index interface.
     *
     * @return Content
     */
    public function index()
    {
        return Admin::content(function (Content $content) {

            $content->header('Články');
            $content->description('Vytváření nových článků');

            $content->body($this->grid());
        });
    }

    /**
     * Show interface.
     *
     * @param $id
     * @return Content
     */
    public function show($id)
    {
        return Admin::content(function (Content $content) use ($id) {

            $content->header('Detail');
            $content->description('detail článku');

            $content->body(Admin::show(Article::findOrFail($id), function (Show $show) {

                $show->id();
                $show->name('Název');
                $show->slug('Odkaz');
                $show->perex('Úvod');
                $show->content('Obsah');
                $show->reference('Reference/Odkazy');
                $show->position('Pořadí');
                $show->created_at();
                $show->updated_at();
            }));
        });
    }

    /**
     * Edit interface.
     *
     * @param $id
     * @return Content
     */
    public function edit($id)
    {
        return Admin::content(function (Content $content) use ($id) {

            $content->header('Úprava stávajícího článku');
            $content->description('Zde upravujte vytvořené články');

            $content->body($this->form()->edit($id));
        });
    }

    /**
     * Create interface.
     *
     * @return Content
     */
    public function create()
    {
        return Admin::content(function (Content $content) {

            $content->header('Nový článek');
            $content->description('Prostor pro vytvoření nového článku');

            $content->body($this->form());
        });
    }

    /**
     * Update article.
     *
     *
     */
    public function update(Request $request, $id)
    {
        $article = $this->article::find($id);

        if($article->name == $request->name)
        {
            $article->update($request->all());
        }
        else
        {
            $article->name = $request->name;
            $article->slug = str_slug($request->name);
            $article->perex = $request->perex;
            $article->content = $request->content;
            $article->reference = $request->reference;
            $article->position = $request->position;
            $article->save(); 
        }
    }

    /**
     * Save article.
     *
     *
     */
    public function store(Request $request)
    {
        $article = $this->article;

        $article->name = $request->name;
        $article->slug = str_slug($request->name);
        $article->perex = $request->perex;
        $article->content = $request->content;
        $article->reference = $request->reference;
        $article->position = $request->position;
        $article->save();
    }

    /**
     * Make a grid builder.
     *
     * @return Grid
     */
    protected function grid()
    {
        return Admin::grid(Article::class, function (Grid $grid) {

            $grid->id('ID')->sortable();
            $grid->column('name');
            $grid->created_at();
            $grid->updated_at();
        });
    }

    /**
     * Make a form builder.
     *
     * @return Form
     */
    protected function form()
    {
        return Admin::form(Article::class, function (Form $form) {

            $articles = $this->article->select('id', 'name')->get()
                ->pluck('name', 'id');
            
            $articles = array_prepend($articles->all(), 'Nemá nadřazený článek');

            $form->display('id', 'ID');
            $form->hidden('id');
            $form->text('name', 'Název článku');
            $form->display('slug', 'Odkaz - bude vytvořen automaticky dle názvu článku');
            $form->ckeditor('perex', 'Úvod');
            $form->ckeditor('content', 'Obsah');
            $form->ckeditor('reference', 'Reference/Odkazy - použijte nečíslovaný seznam');
            $form->select('parent', 'Nadřazený článek')->options($articles);
            $form->number('position', 'Pořadí');
            $form->display('created_at', 'Created At');
            $form->display('updated_at', 'Updated At');

        });
    }
}
