<?php

use Illuminate\Routing\Router;

Admin::registerAuthRoutes();

Route::group([
    'prefix'        => config('admin.route.prefix'),
    'namespace'     => config('admin.route.namespace'),
    'middleware'    => config('admin.route.middleware'),
], function (Router $router) {

    // Articles

    $router->get('/', 'HomeController@index');
    $router->get('/clanky', 'ArticleController@index');
    $router->get('/clanky/create', 'ArticleController@create');
    $router->get('/clanky/{id}', 'ArticleController@show');
    $router->get('/clanky/{id}/edit', 'ArticleController@edit');
    $router->put('/clanky/{id}', 'ArticleController@update');
    $router->post('/clanky', 'ArticleController@store');
    $router->post('/api/users', 'ArticleController@users');

});
