<?php

namespace App\Traits;

trait ArticleStructure 
{

	public $tree;

	public function parseTree($tree, $root = null) 
    {
        $return = array();

        foreach($tree as $child => $parent) 
        {
            if($parent['parent'] == $root) 
            {
                unset($tree[$child]);
                $return[] = array('name' => $parent['name'],
                	'slug' => $parent['slug'],
                    'children' => $this->parseTree($tree, $child));
            }
        }
        return empty($return) ? null : $return;    
    }

    public function printTree($tree) {

        if(!is_null($tree) && count($tree) > 0) {
            $this->tree .= '<ol>';
            foreach($tree as $node) {
                $this->tree .= '<li><a href="'.action('ArticleController@index', ['slug' => $node['slug']]).'" title="'.$node['name'].'">'.$node['name'].'</a>';
                $this->printTree($node['children']);
                $this->tree .= '</li>';
            }
            $this->tree .= '</ol>';
        }
        return $this->tree;
    }

    public function getStructure($articles)
    {
    	$tree = $this->parseTree($articles);
        $tree = $this->printTree($tree);

        return $tree;
    }

}