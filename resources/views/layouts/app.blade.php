<!doctype html>
<html lang="{{ app()->getLocale() }}">
<head>
    @include('includes.head')
</head>
<body class="dash">
    <header>
        @include('includes.header')
    </header>
    <div class="content">
        @if ($errors->any())
        <div class="flash alert alert-danger" style="display:none">
            <ul>
                @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
                @break
                @endforeach
            </ul>
        </div>
        @endif
        @yield('content')
    </div>
    {{-- @include('includes.footer') --}}
</body>
</html>