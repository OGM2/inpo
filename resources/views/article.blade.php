@extends('layouts.app')
@section('content')
	<style type="text/css" media="screen">
	.structure ol { 
		counter-reset: item 
	}
	.structure li { 
		display: block 
	}
	.structure li:before { 
		content: counters(item, ".") " "; 
		counter-increment: item 
	}
</style>
	<div class="structure">
		{!!$tree!!}
	</div>
	<main style="width:920px; margin:0 auto">
		<h1>{{ $article->name }}</h1>
		@if($article->perex)
		<div class="perex">
			{!!$article->perex!!}	
		</div>
		@endif
		<div class="tcontent" style="width:920px; display:block; margin:0 auto">
			{!!$article->content!!}	
		</div>
		@if($article->reference)
		<div class="reference">
			{!!$article->reference!!}
		</div>
		@endif
	</main>
@endsection
